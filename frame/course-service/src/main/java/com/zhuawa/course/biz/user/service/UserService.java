package com.zhuawa.course.biz.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhuawa.course.persistence.user.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author mybatis-plus
 * @since 2019-12-07
 */
public interface UserService extends IService<User> {
    public IPage<User> UserIpageDS(Page page);

}
